package com.jime.wallboxemstechtest.util

import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Test

/**
 * Created by Carlos Jiménez Sánchez  on 22/3/22
 * CJS
 **/

class UnitUtilsTest {

    private val decimalNumber = 1452.56734524

    @Test
    fun testRoundWithPrecision2() {
        val expectedVal = 1452.56

        assertThat(UnitsUtil.roundWithPrecision(decimalNumber, 2), `is`(expectedVal))
    }






}