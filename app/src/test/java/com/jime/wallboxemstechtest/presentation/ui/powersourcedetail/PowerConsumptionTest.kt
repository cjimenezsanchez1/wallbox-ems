package com.jime.wallboxemstechtest.presentation.ui.powersourcedetail

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.mock.MockHistoricList
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers.`is`
import org.hamcrest.MatcherAssert.assertThat
import org.junit.Rule
import org.junit.Test

/**
 * Created by Carlos Jiménez Sánchez  on 22/3/22
 * CJS
 **/

class PowerConsumptionTest {

    private val mockList = MockHistoricList.historicList
    private val viewModel = PowerConsumptionViewModel()

    @get:Rule
    val instantTaskRule = InstantTaskExecutorRule()

    @Test
    fun testAggregatedPowerPerHour() = runBlocking {

        val expectedAggregatedList = arrayListOf<HistoricPowerData>().apply {
            add(
                HistoricPowerData(
                    gridActivePower = 15.0,
                    solarPanelsActivePower = 40.0,
                    quasarsActivePower = 7.0,
                    timeStamp = "2021-09-27T16:45:00+00:00"
                )
            )
            add(
                HistoricPowerData(
                    gridActivePower = 100.0,
                    solarPanelsActivePower = 20.0,
                    quasarsActivePower = 10.0,
                    timeStamp = "2021-09-27T18:45:00+00:00"
                )
            )
            add(
                HistoricPowerData(
                    gridActivePower = 10.0,
                    solarPanelsActivePower = 30.0,
                    quasarsActivePower = 12.0,
                    timeStamp = "2021-09-27T19:43:00+00:00"
                )
            )
        }

        viewModel.getAggregatedPowerData(mockList)
        assertThat(viewModel.aggregatedPowerData.value, `is`(expectedAggregatedList))
    }
}