package com.jime.wallboxemstechtest.mock

import com.jime.wallboxemstechtest.domain.model.HistoricPowerData

/**
 * Created by Carlos Jiménez Sánchez  on 22/3/22
 * CJS
 **/

object MockHistoricList {

    val historicList = arrayListOf<HistoricPowerData>().apply {
        add(HistoricPowerData(gridActivePower = 10.0, solarPanelsActivePower = 30.0, quasarsActivePower = 12.0, timeStamp = "2021-09-27T16:40:00+00:00"))
        add(HistoricPowerData(gridActivePower = 20.0, solarPanelsActivePower = 50.0, quasarsActivePower = 2.0, timeStamp = "2021-09-27T16:45:00+00:00"))
        add(HistoricPowerData(gridActivePower = 90.0, solarPanelsActivePower = 0.0, quasarsActivePower = 15.0, timeStamp = "2021-09-27T18:43:00+00:00"))
        add(HistoricPowerData(gridActivePower = 70.0, solarPanelsActivePower = 30.0, quasarsActivePower = 10.0, timeStamp = "2021-09-27T18:44:00+00:00"))
        add(HistoricPowerData(gridActivePower = 140.0, solarPanelsActivePower = 30.0, quasarsActivePower = 5.0, timeStamp = "2021-09-27T18:45:00+00:00"))
        add(HistoricPowerData(gridActivePower = 10.0, solarPanelsActivePower = 30.0, quasarsActivePower = 12.0, timeStamp = "2021-09-27T19:40:00+00:00"))
        add(HistoricPowerData(gridActivePower = 10.0, solarPanelsActivePower = 30.0, quasarsActivePower = 12.0, timeStamp = "2021-09-27T19:43:00+00:00"))
    }



}