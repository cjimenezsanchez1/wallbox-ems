package com.jime.wallboxemstechtest.mock

import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.repository.EmsRepository

/**
 * Created by Carlos Jiménez Sánchez  on 22/3/22
 * CJS
 **/

class FakeEmsRepository(): EmsRepository {

    private val mockApiData = CurrentPowerData(
        buildingDemand = 123.54,
        gridPower = 92.65,
        solarPower = 4.45,
        quasarsPower = -26.44)

    private val mockDbData = CurrentPowerData(
        buildingDemand = 101.90,
        gridPower = 78.5,
        solarPower = 12.32,
        quasarsPower = -11.08
    )

    override suspend fun fetchCurrentPowerData(fromCache: Boolean): CurrentPowerData {
        return if (fromCache) {
            mockDbData
        } else {
            mockApiData
        }
    }

    override suspend fun saveCurrentPowerData(currentPowerData: CurrentPowerData) {
        TODO("Not yet implemented")
    }

    override suspend fun fetchHistoricPowerData(fromCache: Boolean): List<HistoricPowerData> {
        TODO("Not yet implemented")
    }

    override suspend fun saveHistoricPowerData(historicList: List<HistoricPowerData>) {
        TODO("Not yet implemented")
    }

}