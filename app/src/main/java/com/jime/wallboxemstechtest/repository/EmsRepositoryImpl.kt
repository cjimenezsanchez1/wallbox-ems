package com.jime.wallboxemstechtest.repository

import com.jime.wallboxemstechtest.data.EmsLocalDataSource
import com.jime.wallboxemstechtest.data.EmsRemoteDataSource
import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataMapper
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataMapper
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

class EmsRepositoryImpl @Inject constructor(
    private val emsRemoteDataSource: EmsRemoteDataSource,
    private val emsLocalDataSource: EmsLocalDataSource,
    private val currentPowerDataMapper: CurrentPowerDataMapper,
    private val historicPowerDataMapper: HistoricPowerDataMapper
) : EmsRepository {

    companion object {
        private const val TAG = "EmsRepositoryImpl"
    }

    override suspend fun fetchCurrentPowerData(fromCache: Boolean): CurrentPowerData {
        return if (fromCache) {
            currentPowerDataMapper.mapToDomainModel(emsLocalDataSource.fetchCurrentPowerData())
        } else {
            currentPowerDataMapper.mapToDomainModel(emsRemoteDataSource.fetchCurrentPowerData())
        }
    }

    override suspend fun saveCurrentPowerData(currentPowerData: CurrentPowerData) {
        emsLocalDataSource.insertCurrentPowerData(
            currentPowerDataMapper.mapFromDomainModel(currentPowerData)
        )
    }

    override suspend fun fetchHistoricPowerData(fromCache: Boolean): List<HistoricPowerData> {
        return if (fromCache) {
            historicPowerDataMapper.toDomainList(emsLocalDataSource.fetchHistoricPowerData())
        } else {
            historicPowerDataMapper.toDomainList(emsRemoteDataSource.fetchHistoricPowerData())
        }
    }

    override suspend fun saveHistoricPowerData(historicList: List<HistoricPowerData>) {
        emsLocalDataSource.insertHistoricPowerData(
            historicPowerDataMapper.fromDomainList(historicList)
        )
    }
}