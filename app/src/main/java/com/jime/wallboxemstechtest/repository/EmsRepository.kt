package com.jime.wallboxemstechtest.repository

import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

interface EmsRepository {

    suspend fun fetchCurrentPowerData(fromCache: Boolean) : CurrentPowerData

    suspend fun saveCurrentPowerData(currentPowerData: CurrentPowerData)

    suspend fun fetchHistoricPowerData(fromCache: Boolean): List<HistoricPowerData>

    suspend fun saveHistoricPowerData(historicList: List<HistoricPowerData>)
}