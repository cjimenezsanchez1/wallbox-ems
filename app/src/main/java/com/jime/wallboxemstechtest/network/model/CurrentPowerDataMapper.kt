package com.jime.wallboxemstechtest.network.model

import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.util.DomainMapper

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

class CurrentPowerDataMapper() : DomainMapper<CurrentPowerDataDto, CurrentPowerData> {
    override fun mapToDomainModel(model: CurrentPowerDataDto): CurrentPowerData {
        return CurrentPowerData(
            solarPower = model.solarPower,
            quasarsPower = model.quarsarPower,
            gridPower = model.gridPower,
            buildingDemand = model.buildingDemand,
            systemSocPower = model.systemSoc,
            totalEnergy = model.totalEnergy,
            currentEnergy = model.currentEnergy
        )
    }

    override fun mapFromDomainModel(domainModel: CurrentPowerData): CurrentPowerDataDto {
        return CurrentPowerDataDto(
            solarPower = domainModel.solarPower,
            quarsarPower = domainModel.quasarsPower,
            gridPower = domainModel.gridPower,
            buildingDemand = domainModel.buildingDemand,
            systemSoc = domainModel.systemSocPower,
            totalEnergy = domainModel.totalEnergy,
            currentEnergy = domainModel.currentEnergy
        )
    }
}
