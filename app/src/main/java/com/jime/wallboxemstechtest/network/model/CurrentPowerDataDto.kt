package com.jime.wallboxemstechtest.network.model

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

import com.google.gson.annotations.SerializedName

data class CurrentPowerDataDto(
    @SerializedName("solar_power") val solarPower: Double,
    @SerializedName("quasars_power") val quarsarPower: Double,
    @SerializedName("grid_power") val gridPower: Double,
    @SerializedName("building_demand") val buildingDemand: Double,
    @SerializedName("system_soc") val systemSoc: Double,
    @SerializedName("total_energy") val totalEnergy: Double,
    @SerializedName("current_energy") val currentEnergy: Double)
