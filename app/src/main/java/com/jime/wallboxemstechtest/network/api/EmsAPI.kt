package com.jime.wallboxemstechtest.network.api

import com.jime.wallboxemstechtest.network.model.CurrentPowerDataDto
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataDto
import retrofit2.http.GET
import retrofit2.http.Headers

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

interface EmsAPI {

    @Headers("mocks:true")
    @GET("api/v1/historic_data")
    suspend fun fetchHistoricPowerData(): List<HistoricPowerDataDto>

    @Headers("mocks:true")
    @GET("api/v1/live_data")
    suspend fun fetchCurrentPowerData(): CurrentPowerDataDto

}