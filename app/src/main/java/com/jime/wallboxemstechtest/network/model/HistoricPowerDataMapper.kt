package com.jime.wallboxemstechtest.network.model

import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.domain.util.DomainMapper

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

class HistoricPowerDataMapper : DomainMapper<HistoricPowerDataDto, HistoricPowerData> {

    override fun mapToDomainModel(model: HistoricPowerDataDto): HistoricPowerData {
        return HistoricPowerData(
            buildingActivePower = model.buildActivePower,
            gridActivePower = model.gridActivePower,
            solarPanelsActivePower = model.pvActivePower,
            quasarsActivePower = model.quasarsActivePower,
            timeStamp = model.timeStamp
        )
    }

    override fun mapFromDomainModel(domainModel: HistoricPowerData): HistoricPowerDataDto {
        return HistoricPowerDataDto(
            buildActivePower = domainModel.buildingActivePower,
            gridActivePower = domainModel.gridActivePower,
            pvActivePower = domainModel.solarPanelsActivePower,
            quasarsActivePower = domainModel.quasarsActivePower,
            timeStamp = domainModel.timeStamp
        )
    }

    fun toDomainList(list: List<HistoricPowerDataDto>): List<HistoricPowerData> {
        return list.map { mapToDomainModel(it) }
    }

    fun fromDomainList(list: List<HistoricPowerData>): List<HistoricPowerDataDto> {
        return list.map { mapFromDomainModel(it) }
    }
}