package com.jime.wallboxemstechtest.network.api

import android.content.Context
import com.jime.wallboxemstechtest.network.MockRequestInterceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

class ApiGenerator @Inject constructor() {

    companion object {
        private const val BASE_URL = "https://wallbox/api/"
    }

    fun <API> buildApi(context: Context, api: Class<API>): API {

        val client = OkHttpClient.Builder()
            .connectTimeout(60L, TimeUnit.SECONDS)
            .readTimeout(60L, TimeUnit.SECONDS)
            .addInterceptor(MockRequestInterceptor(context))
            .addInterceptor(getLoggingInterceptor())
            .build()

        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .client(client)
            .build()
            .create(api)
    }

    private fun getLoggingInterceptor(): HttpLoggingInterceptor {
        val loggingInterceptorRetrofit = HttpLoggingInterceptor()
        loggingInterceptorRetrofit.level = HttpLoggingInterceptor.Level.BODY
        return loggingInterceptorRetrofit
    }


}