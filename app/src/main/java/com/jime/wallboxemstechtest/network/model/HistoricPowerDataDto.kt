package com.jime.wallboxemstechtest.network.model

import com.google.gson.annotations.SerializedName

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

data class HistoricPowerDataDto(
    @SerializedName("building_active_power") val buildActivePower: Double,
    @SerializedName("grid_active_power") val gridActivePower: Double,
    @SerializedName("pv_active_power") val pvActivePower: Double,
    @SerializedName("quasars_active_power") val quasarsActivePower: Double,
    @SerializedName("timestamp") val timeStamp: String)
