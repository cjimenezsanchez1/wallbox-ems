package com.jime.wallboxemstechtest.util

import kotlin.math.pow

/**
 * Created by Carlos Jiménez Sánchez  on 19/3/22
 * CJS
 **/

const val KW_UNITS_TEXT = "kW"
const val KWH_UNITS_TEXT = "kWh"
const val PERCENTAGE_TEXT = "%"
const val HOUR_UNIT = "h"

object UnitsUtil {

    fun roundWithPrecision(number: Double, precision: Int = 2): Double {
        val decimalPart = (number * 10.0.pow(precision)).toInt()
        return decimalPart / 10.0.pow(precision)
    }

    fun getKWTextValue(value: Double): String {
        return "${roundWithPrecision(value)} $KW_UNITS_TEXT"
    }

    fun getKWhTextValue(value: Double): String {
        return "${roundWithPrecision(value)} $KWH_UNITS_TEXT"
    }

    fun getPercentageTextValue(value: Double): String {
        return "${roundWithPrecision(value)} $PERCENTAGE_TEXT"
    }

    fun getHourString(hour: Int): String {
        return if (hour < 10) {
            "0$hour $HOUR_UNIT"
        }else {
            "$hour $HOUR_UNIT"
        }
    }
}