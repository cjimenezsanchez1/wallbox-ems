package com.jime.wallboxemstechtest.cache

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataDto
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataDto
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.IOException
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 21/3/22
 * CJS
 **/

class EmsDaoImpl @Inject constructor(@ApplicationContext private val context: Context) : EmsDao {

    override suspend fun fetchCurrentPowerData(): CurrentPowerDataDto {
        //FAKE DATABASE
        val jsonString = getJsonDataFromAsset(context, "mocks/live_data.json")
        val gson = Gson()
        return gson.fromJson(jsonString, CurrentPowerDataDto::class.java)
    }

    override suspend fun fetchHistoricPowerData(): List<HistoricPowerDataDto> {
        //FAKE DATABASE
        val jsonString = getJsonDataFromAsset(context, "mocks/historic_data.json")
        val listPersonType = object : TypeToken<List<HistoricPowerDataDto>>() {}.type
        val gson = Gson()
        return gson.fromJson(jsonString, listPersonType)
    }

    override suspend fun insertCurrentPowerData(currentPowerDataDto: CurrentPowerDataDto) {
        //TODO implement
    }

    override suspend fun insertHistoricPowerData(historicList: List<HistoricPowerDataDto>) {
        //TODO implement
    }

    private fun getJsonDataFromAsset(context: Context, fileName: String): String? {
        val jsonString: String
        try {
            jsonString = context.assets.open(fileName).bufferedReader().use { it.readText() }
        } catch (ioException: IOException) {
            ioException.printStackTrace()
            return null
        }
        return jsonString
    }


}