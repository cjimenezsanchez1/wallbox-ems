package com.jime.wallboxemstechtest.cache

import com.jime.wallboxemstechtest.network.model.CurrentPowerDataDto
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataDto

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

interface EmsDao {

    suspend fun fetchCurrentPowerData(): CurrentPowerDataDto

    suspend fun fetchHistoricPowerData(): List<HistoricPowerDataDto>

    suspend fun insertCurrentPowerData(currentPowerDataDto: CurrentPowerDataDto)

    suspend fun insertHistoricPowerData(historicList: List<HistoricPowerDataDto>)

}