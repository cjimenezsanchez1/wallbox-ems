package com.jime.wallboxemstechtest.data

import com.jime.wallboxemstechtest.network.api.EmsAPI
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataDto
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataDto
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

class EmsRemoteDataSource @Inject constructor(
    private val emsApi: EmsAPI) {

    suspend fun fetchCurrentPowerData(): CurrentPowerDataDto = emsApi.fetchCurrentPowerData()

    suspend fun fetchHistoricPowerData(): List<HistoricPowerDataDto> =
        emsApi.fetchHistoricPowerData()
}
