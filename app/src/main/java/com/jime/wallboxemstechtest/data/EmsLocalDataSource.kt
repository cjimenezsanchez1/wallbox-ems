package com.jime.wallboxemstechtest.data

import com.jime.wallboxemstechtest.cache.EmsDao
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataDto
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataDto
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

class EmsLocalDataSource @Inject constructor(private val emsDao: EmsDao) {

    suspend fun fetchCurrentPowerData(): CurrentPowerDataDto {
        return emsDao.fetchCurrentPowerData()
    }

    suspend fun fetchHistoricPowerData(): List<HistoricPowerDataDto> {
        return emsDao.fetchHistoricPowerData()
    }

    suspend fun insertCurrentPowerData(currentPowerDataDto: CurrentPowerDataDto) {
        emsDao.insertCurrentPowerData(currentPowerDataDto)
    }

    suspend fun insertHistoricPowerData(historicList: List<HistoricPowerDataDto>) {
        emsDao.insertHistoricPowerData(historicList)
    }
}