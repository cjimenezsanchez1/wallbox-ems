package com.jime.wallboxemstechtest

import android.app.Application
import dagger.hilt.android.HiltAndroidApp

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

@HiltAndroidApp
class WallboxApp: Application() {
}