package com.jime.wallboxemstechtest.presentation.ui.dashboard

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.jime.wallboxemstechtest.domain.dataState.DataState
import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.repository.EmsRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

@HiltViewModel
class DashboardViewModel @Inject constructor(private val emsRepository: EmsRepository) :
    ViewModel() {

    companion object {
        private const val TAG = "DashboardViewModel"
    }

    var currentPowerData: MutableLiveData<DataState<CurrentPowerData>> = MutableLiveData()
    var historicPowerData: MutableLiveData<DataState<List<HistoricPowerData>>> = MutableLiveData()

   private var noInternet = true

    fun fetchCurrentPowerData() {
        viewModelScope.launch {
            currentPowerData.value = DataState.loading()
            try {
                if (noInternet) {
                    //fetch data from local db
                    currentPowerData.value =
                        DataState.success(emsRepository.fetchCurrentPowerData(true))
                } else {
                    //fetch data from api
                    delay(1000)
                    val data = emsRepository.fetchCurrentPowerData(false)
                    //save data in the local db
                    emsRepository.saveCurrentPowerData(data)
                    //post saved data to currentPower liveData
                    currentPowerData.value =
                        DataState.success(emsRepository.fetchCurrentPowerData(true))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                currentPowerData.value = DataState.error(e.message ?: "Unknown Error")
            }
        }
    }

    fun fetchHistoricPowerData() {
        viewModelScope.launch {
            historicPowerData.value = DataState.loading()

            try {
                if (noInternet) {
                    //fetch data from local db
                    historicPowerData.value =
                        DataState.success(emsRepository.fetchHistoricPowerData(true))
                } else {
                    //fetch data from api
                    delay(1000)
                    val data = emsRepository.fetchHistoricPowerData(false)
                    //save data in the local db
                    emsRepository.saveHistoricPowerData(data)
                    //post saved data to historicPower liveData
                    historicPowerData.value =
                        DataState.success(emsRepository.fetchHistoricPowerData(true))
                }
            } catch (e: Exception) {
                e.printStackTrace()
                historicPowerData.value = DataState.error(e.message ?: "Unknown Error")
            }
        }
    }

    fun getQuasarsDischargedEnergy(historicData: List<HistoricPowerData>): Double {
        var sumOfPowers = 0.0
        historicData.forEach {
            if (it.quasarsActivePower < 0) {
                sumOfPowers += it.quasarsActivePower
            }
        }
        return (sumOfPowers / 60) * (-1)
    }

    fun getQuasarsChargedEnergy(historicData: List<HistoricPowerData>): Double {
        var sumOfPowers = 0.0
        historicData.forEach {
            if (it.quasarsActivePower > 0) {
                sumOfPowers += it.quasarsActivePower
            }
        }
        return sumOfPowers / 60
    }

    fun getHistoricPowerDataList(): ArrayList<HistoricPowerData> {
        return historicPowerData.value?.data?.toCollection(ArrayList()) ?: arrayListOf()
    }
}