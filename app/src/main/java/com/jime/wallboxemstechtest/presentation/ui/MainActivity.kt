package com.jime.wallboxemstechtest.presentation.ui

import android.os.Bundle
import com.jime.wallboxemstechtest.base.BaseActivity
import com.jime.wallboxemstechtest.databinding.ActivityMainBinding
import com.jime.wallboxemstechtest.presentation.ui.dashboard.DashboardFragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        setInitialFragment()
    }

    private fun setInitialFragment() {
        if (supportFragmentManager.backStackEntryCount == 0) {
            navigateTo(
                DashboardFragment.newInstance(),
                DashboardFragment.TAG,
                slideIn = false,
                addToBackStack = false
            )
        }
    }
}