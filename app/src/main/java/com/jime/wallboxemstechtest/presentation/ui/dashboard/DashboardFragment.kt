package com.jime.wallboxemstechtest.presentation.ui.dashboard

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.github.mikephil.charting.charts.PieChart
import com.github.mikephil.charting.data.PieData
import com.github.mikephil.charting.data.PieDataSet
import com.github.mikephil.charting.data.PieEntry
import com.github.mikephil.charting.formatter.PercentFormatter
import com.github.mikephil.charting.listener.ChartTouchListener
import com.jime.wallboxemstechtest.R
import com.jime.wallboxemstechtest.databinding.DashboardFragmentBinding
import com.jime.wallboxemstechtest.domain.model.CurrentPowerData
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.presentation.ui.MainActivity
import com.jime.wallboxemstechtest.presentation.ui.powersourcedetail.PowerConsumptionFragment
import com.jime.wallboxemstechtest.util.UnitsUtil
import dagger.hilt.android.AndroidEntryPoint

/**
 * Created by Carlos Jiménez Sánchez  on 20/3/22
 * CJS
 **/

@AndroidEntryPoint
class DashboardFragment : Fragment() {

    companion object {
        const val TAG = "DashboardFragment"
        const val ARG_HISTORIC_POWER_DATA = "ARG_HISTORIC_POWER_DATA"

        fun newInstance(): DashboardFragment {
            return DashboardFragment()
        }
    }

    private val viewModel: DashboardViewModel by viewModels()
    private var _binding: DashboardFragmentBinding? = null
    private lateinit var chartView: PieChart
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = DashboardFragmentBinding.inflate(inflater, container, false)

        chartView = binding.root.findViewById(R.id.power_percentage_pie_chart)
        initOnClickListeners()
        initObservers()
        fetchDashboardData()

        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun initOnClickListeners() {
        chartView.onTouchListener = object : ChartTouchListener<PieChart>(chartView) {
            override fun onTouch(p0: View?, p1: MotionEvent?): Boolean {
                if (viewModel.currentPowerData.value?.loading == false) {
                    navigateToPowerConsumptionDetail()
                }
                return false
            }
        }

        binding.powerDataRetryButton.setOnClickListener {
            binding.powerDataErrorContainer.visibility = View.INVISIBLE
            viewModel.fetchCurrentPowerData()
        }
    }

    private fun initObservers() {
        viewModel.currentPowerData.observe(viewLifecycleOwner) { result ->
            updateProgressBarVisibility(result.loading)

            result.data?.let { power ->
                setCurrentPowerWidget(power)
                setCurrentPowerChartWidget(power)
            }

            result.error?.let { e ->
                onPowerDataFetchError(e)
            }
        }

        viewModel.historicPowerData.observe(viewLifecycleOwner) { result ->
            result.data?.let { historicList ->
                setQuasarsEnergyConsumption(historicList)
            }
            result.error?.let {
                //handle data fetch error
            }
        }
    }

    private fun fetchDashboardData() {
        viewModel.fetchCurrentPowerData()
        viewModel.fetchHistoricPowerData()
    }

    private fun updateProgressBarVisibility(loading: Boolean) {
        binding.chartProgressBar.visibility = if (loading) View.VISIBLE else View.GONE
        binding.powerChartDataContainer.visibility = if (loading) View.INVISIBLE else View.VISIBLE
    }

    private fun setCurrentPowerWidget(cP: CurrentPowerData) {
        binding.quasarsCurrentPower.text = UnitsUtil.getKWTextValue(cP.quasarsPower * (-1))
        binding.solarPanelsCurrentPower.text = UnitsUtil.getKWTextValue(cP.solarPower)
        binding.gridCurrentPower.text = UnitsUtil.getKWTextValue(cP.gridPower)
        binding.buildingDemand.text = UnitsUtil.getKWTextValue(cP.buildingDemand)
    }

    private fun setCurrentPowerChartWidget(cP: CurrentPowerData) {

        val chartEntries = arrayListOf<PieEntry>()

        val valuesMap: MutableMap<String, Double> = mutableMapOf()
        valuesMap[getString(R.string.solar_panels)] = cP.solarPowerPercentage()
        valuesMap[getString(R.string.quasars_power)] = cP.quasarPowerPercentage() * (-1)
        valuesMap[getString(R.string.grid_power)] = cP.gridPowerPercentage()

        val colors = arrayListOf<Int>()
        colors.add(ResourcesCompat.getColor(this.resources, R.color.solar_power_color, null))
        colors.add(ResourcesCompat.getColor(this.resources, R.color.quasars_power_color, null))
        colors.add(ResourcesCompat.getColor(this.resources, R.color.grid_power_color, null))

        valuesMap.forEach { chartEntries.add(PieEntry(it.value.toFloat(), it.key)) }

        val pieDataSet = PieDataSet(chartEntries, "")
        pieDataSet.colors = colors
        pieDataSet.valueTextSize = 14f
        pieDataSet.setDrawValues(false)

        val pieData = PieData(pieDataSet)
        pieData.setValueFormatter(PercentFormatter())

        chartView.data = pieData
        chartView.description.isEnabled = false
        chartView.legend.isEnabled = false
        chartView.animateXY(500, 500)
        chartView.invalidate()

        binding.solarPanelsCurrentPowerPercentage.text =
            UnitsUtil.getPercentageTextValue(cP.solarPowerPercentage())
        binding.quasarsCurrentPowerPercentage.text =
            UnitsUtil.getPercentageTextValue(cP.quasarPowerPercentage() * (-1))
        binding.gridPanelsCurrentPowerPercentage.text =
            UnitsUtil.getPercentageTextValue(cP.gridPowerPercentage())
    }

    private fun onPowerDataFetchError(errorMessage: String) {
        binding.powerDataErrorContainer.visibility = View.VISIBLE
        binding.powerPercentagePieChart.visibility = View.GONE
        Log.d(TAG, "onPowerDataFetchError: $errorMessage")
    }

    private fun setQuasarsEnergyConsumption(historicPowerList: List<HistoricPowerData>) {
        binding.quasarsEnergyDischarged.text =
            UnitsUtil.getKWhTextValue(viewModel.getQuasarsDischargedEnergy(historicPowerList))
        binding.quasarsEnergyCharged.text =
            UnitsUtil.getKWhTextValue(viewModel.getQuasarsChargedEnergy(historicPowerList))
    }

    private fun navigateToPowerConsumptionDetail() {
        activity?.let { act ->
            (act as MainActivity).navigateTo(
                PowerConsumptionFragment.newInstance(viewModel.getHistoricPowerDataList()), TAG
            )
        }
    }

}