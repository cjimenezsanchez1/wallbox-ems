package com.jime.wallboxemstechtest.presentation.ui.powersourcedetail

import android.graphics.Color
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import com.github.mikephil.charting.charts.BarChart
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.components.YAxis
import com.github.mikephil.charting.data.BarData
import com.github.mikephil.charting.data.BarDataSet
import com.github.mikephil.charting.data.BarEntry
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter
import com.github.mikephil.charting.formatter.ValueFormatter
import com.github.mikephil.charting.interfaces.datasets.IBarDataSet
import com.jime.wallboxemstechtest.R
import com.jime.wallboxemstechtest.databinding.PowerConsumptionFragmentBinding
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import com.jime.wallboxemstechtest.presentation.ui.dashboard.DashboardFragment.Companion.ARG_HISTORIC_POWER_DATA
import com.jime.wallboxemstechtest.util.UnitsUtil
import dagger.hilt.android.AndroidEntryPoint
import java.time.OffsetDateTime

/**
 * Created by Carlos Jiménez Sánchez  on 20/3/22
 * CJS
 **/

@AndroidEntryPoint
class PowerConsumptionFragment : Fragment() {

    companion object {
        const val TAG = "PowerConsumptionFragment"

        fun newInstance(historicPowerList: ArrayList<HistoricPowerData>): PowerConsumptionFragment {
            return PowerConsumptionFragment().apply {
                arguments = Bundle(2).apply {
                    putParcelableArrayList(ARG_HISTORIC_POWER_DATA, historicPowerList)
                }
            }
        }
    }

    private var _binding: PowerConsumptionFragmentBinding? = null
    private val binding get() = _binding!!

    private lateinit var barChart: BarChart

    private var historicPowerList: ArrayList<HistoricPowerData> = arrayListOf()
    private val viewModel: PowerConsumptionViewModel by viewModels()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PowerConsumptionFragmentBinding.inflate(inflater, container, false)

        barChart = binding.root.findViewById(R.id.power_bar_chart)

        @Suppress("UNCHECKED_CAST")
        historicPowerList =
            arguments?.get(ARG_HISTORIC_POWER_DATA) as ArrayList<HistoricPowerData>?
                ?: arrayListOf()

        initObservers()
        getPowerChartData()
        return binding.root
    }

    private fun initObservers() {
        viewModel.aggregatedPowerData.observe(viewLifecycleOwner) { aList ->
            Log.d(TAG, "initObservers")
            initBarChartWidget(aList)
        }
    }

    private fun getPowerChartData() {
        viewModel.getAggregatedPowerData(historicPowerList)
    }

    private fun initBarChartWidget(aggregatedList: List<HistoricPowerData>) {

        val gridEntries = arrayListOf<BarEntry>()
        val quasarsEntries = arrayListOf<BarEntry>()
        val solarPanelEntries = arrayListOf<BarEntry>()

        for (i in 0..aggregatedList.lastIndex) {
            gridEntries.add(BarEntry(i.toFloat(), aggregatedList[i].gridActivePower.toFloat()))
            solarPanelEntries.add(
                BarEntry(
                    i.toFloat(),
                    aggregatedList[i].solarPanelsActivePower.toFloat()
                )
            )
            quasarsEntries.add(
                BarEntry(
                    i.toFloat(),
                    aggregatedList[i].quasarsActivePower.toFloat()
                )
            )
        }

        val gridBarDataSet = BarDataSet(gridEntries, "grid")
        gridBarDataSet.color =
            ResourcesCompat.getColor(this.resources, R.color.grid_power_color, null)

        val solarPanelBarDataSet = BarDataSet(solarPanelEntries, "solar")
        solarPanelBarDataSet.color =
            ResourcesCompat.getColor(this.resources, R.color.solar_power_color, null)

        val quasarsBarDataSet = BarDataSet(quasarsEntries, "quasars")
        quasarsBarDataSet.color =
            ResourcesCompat.getColor(this.resources, R.color.quasars_power_color, null)

        val labels = arrayListOf<String>()
        for (v in aggregatedList) {
            labels.add(UnitsUtil.getHourString(OffsetDateTime.parse(v.timeStamp).hour))
        }

        gridBarDataSet.setDrawValues(false)
        solarPanelBarDataSet.setDrawValues(false)
        quasarsBarDataSet.setDrawValues(false)

        val dataSets = ArrayList<IBarDataSet>()
        dataSets.add(gridBarDataSet)
        dataSets.add(solarPanelBarDataSet)
        dataSets.add(quasarsBarDataSet)

        val barData = BarData(dataSets)

        val groupSpace = 0.04f
        val barSpace = 0.02f
        val barWidth = 0.3f

        barData.barWidth = barWidth
        val xAxis: XAxis = barChart.xAxis
        xAxis.valueFormatter = IndexAxisValueFormatter(labels)
        xAxis.axisMaximum = gridEntries.size.toFloat() + 0.25f

        xAxis.setCenterAxisLabels(true)
        xAxis.position = XAxis.XAxisPosition.BOTTOM
        xAxis.setDrawGridLines(false)
        xAxis.granularity = 1f
        xAxis.textColor = Color.BLACK
        xAxis.textSize = 12f
        xAxis.axisLineColor = Color.WHITE

        val leftAxis: YAxis = barChart.axisLeft
        leftAxis.textColor = Color.BLACK
        leftAxis.textSize = 12f
        leftAxis.spaceTop = 30f
        leftAxis.valueFormatter = object : ValueFormatter() {
            override fun getFormattedValue(value: Float): String {
                return "${value.toInt()} kW"
            }
        }
        leftAxis.setPosition(YAxis.YAxisLabelPosition.OUTSIDE_CHART)

        val rightAxis: YAxis = barChart.axisRight
        rightAxis.setDrawGridLines(false)
        rightAxis.isEnabled = false

        barChart.data = barData
        barChart.setScaleEnabled(false)
        barChart.setVisibleXRangeMaximum(5f)
        barChart.groupBars(0.25f, groupSpace, barSpace)

        barChart.description.isEnabled = false
        barChart.legend.isEnabled = false
        barChart.moveViewToX((gridEntries.size - 1).toFloat())
        barChart.invalidate()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}