package com.jime.wallboxemstechtest.presentation.ui.powersourcedetail

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.jime.wallboxemstechtest.domain.model.HistoricPowerData
import dagger.hilt.android.lifecycle.HiltViewModel
import java.time.OffsetDateTime
import javax.inject.Inject

/**
 * Created by Carlos Jiménez Sánchez  on 21/3/22
 * CJS
 **/

@HiltViewModel
class PowerConsumptionViewModel @Inject constructor() : ViewModel() {

    var aggregatedPowerData: MutableLiveData<List<HistoricPowerData>> = MutableLiveData()

    fun getAggregatedPowerData(historicList: List<HistoricPowerData>) {

        val calculatedList = arrayListOf<HistoricPowerData>()

        var hour = -1
        var numbOfSamples = 0
        var sumGrid = 0.0
        var sumQuasars = 0.0
        var sumSolarPanels = 0.0

        for (i in 0..historicList.lastIndex) {
            val value = historicList[i]
            val valueHour = OffsetDateTime.parse(value.timeStamp).hour
            if (valueHour != hour || i == historicList.lastIndex) {
                if (numbOfSamples != 0) {

                    val powerAggregate = HistoricPowerData(
                        gridActivePower = sumGrid / numbOfSamples,
                        solarPanelsActivePower = sumSolarPanels / numbOfSamples,
                        quasarsActivePower = sumQuasars / numbOfSamples,
                        timeStamp = if (i != historicList.lastIndex) historicList[i - 1].timeStamp
                        else historicList[i].timeStamp
                    )

                    calculatedList.add(powerAggregate)

                    //reset accumulative values
                    numbOfSamples = 1
                    sumGrid = 0.0
                    sumQuasars = 0.0
                    sumSolarPanels = 0.0
                    sumGrid += value.gridActivePower
                    sumQuasars += value.quasarsActivePower
                    sumSolarPanels += value.solarPanelsActivePower
                    hour = valueHour
                } else {
                    hour = valueHour
                    numbOfSamples += 1
                    sumGrid += value.gridActivePower
                    sumQuasars += value.quasarsActivePower
                    sumSolarPanels += value.solarPanelsActivePower
                }
            } else {
                numbOfSamples += 1
                sumGrid += value.gridActivePower
                sumQuasars += value.quasarsActivePower
                sumSolarPanels += value.solarPanelsActivePower
            }
        }
        aggregatedPowerData.value = calculatedList
    }
}