package com.jime.wallboxemstechtest.base

import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.jime.wallboxemstechtest.R

/**
 * Created by Carlos Jiménez Sánchez  on 21/3/22
 * CJS
 **/

abstract class BaseActivity : AppCompatActivity() {

    fun navigateTo(
        fragment: Fragment,
        tag: String,
        slideIn: Boolean = true,
        addToBackStack: Boolean = true
    ) {
        val ft = supportFragmentManager.beginTransaction()

        if (slideIn) {
            ft.setCustomAnimations(
                R.anim.slide_in_right,
                R.anim.slide_out_left,
                R.anim.slide_in_left,
                R.anim.slide_out_right
            )
        }
        ft.add(R.id.main_frame, fragment, tag)
        if (addToBackStack) {
            ft.addToBackStack(tag)
        }
        ft.commit()
    }

}