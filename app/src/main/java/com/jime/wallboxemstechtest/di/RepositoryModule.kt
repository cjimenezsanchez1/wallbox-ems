package com.jime.wallboxemstechtest.di

import com.jime.wallboxemstechtest.data.EmsLocalDataSource
import com.jime.wallboxemstechtest.data.EmsRemoteDataSource
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataMapper
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataMapper
import com.jime.wallboxemstechtest.repository.EmsRepository
import com.jime.wallboxemstechtest.repository.EmsRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {

    @Provides
    @Singleton
    fun providesEmsRepository(
        emsRemoteDataSource: EmsRemoteDataSource,
        emsLocalDataSource: EmsLocalDataSource,
        currentPowerDataMapper: CurrentPowerDataMapper,
        historicPowerDataMapper: HistoricPowerDataMapper
    ): EmsRepository {
        return EmsRepositoryImpl(
            emsRemoteDataSource,
            emsLocalDataSource,
            currentPowerDataMapper,
            historicPowerDataMapper
        )
    }

}