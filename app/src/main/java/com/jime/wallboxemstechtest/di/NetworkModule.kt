package com.jime.wallboxemstechtest.di

import android.content.Context
import com.jime.wallboxemstechtest.cache.EmsDao
import com.jime.wallboxemstechtest.data.EmsLocalDataSource
import com.jime.wallboxemstechtest.data.EmsRemoteDataSource
import com.jime.wallboxemstechtest.network.api.ApiGenerator
import com.jime.wallboxemstechtest.network.api.EmsAPI
import com.jime.wallboxemstechtest.network.model.CurrentPowerDataMapper
import com.jime.wallboxemstechtest.network.model.HistoricPowerDataMapper
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton


/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {

    @Provides
    @Singleton
    fun providesEmsApi(
        @ApplicationContext context: Context,
        apiGenerator: ApiGenerator
    ): EmsAPI {
        return apiGenerator.buildApi(context, EmsAPI::class.java)
    }

    @Provides
    fun providesCurrentPowerDataMapper(): CurrentPowerDataMapper {
        return CurrentPowerDataMapper()
    }

    @Provides
    fun providesHistoricPowerDataMapper(): HistoricPowerDataMapper {
        return HistoricPowerDataMapper()
    }

    @Provides
    fun providesEmsRemoteDataSource(emsAPI: EmsAPI): EmsRemoteDataSource {
        return EmsRemoteDataSource(emsAPI)
    }

    @Provides
    fun providesEmsLocalDataSource(
        emsDao: EmsDao
    ): EmsLocalDataSource {
        return EmsLocalDataSource(emsDao)
    }


}