package com.jime.wallboxemstechtest.di

/**
 * Created by Carlos Jiménez Sánchez  on 18/3/22
 * CJS
 **/

import android.content.Context
import com.jime.wallboxemstechtest.WallboxApp
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object AppModule {

    @Singleton
    @Provides
    fun provideApplication(@ApplicationContext app: Context): WallboxApp {
        return app as WallboxApp
    }

}