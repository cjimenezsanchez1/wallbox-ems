package com.jime.wallboxemstechtest.di

import com.jime.wallboxemstechtest.cache.EmsDao
import com.jime.wallboxemstechtest.cache.EmsDaoImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent

/**
 * Created by Carlos Jiménez Sánchez  on 21/3/22
 * CJS
 **/

@Module
@InstallIn(SingletonComponent::class)
abstract class LocalDataModule {

    @Binds
    abstract fun binEmsDAO(
        emsDao: EmsDaoImpl
    ): EmsDao
}