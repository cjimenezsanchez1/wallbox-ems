package com.jime.wallboxemstechtest.domain.dataState

/**
 * Created by Carlos Jiménez Sánchez  on 21/3/22
 * CJS
 **/

data class DataState<out T>(
    val data: T? = null,
    val error: String? = null,
    val loading: Boolean = false
) {

    companion object {

        fun <T> success(data: T): DataState<T> {
            return DataState(data = data)
        }

        fun <T> error(errorMessage: String): DataState<T> {
            return DataState(error = errorMessage)
        }

        fun <T> loading(): DataState<T> =
            DataState(loading = true)
    }

}