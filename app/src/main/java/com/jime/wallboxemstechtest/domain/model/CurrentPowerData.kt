package com.jime.wallboxemstechtest.domain.model

import com.jime.wallboxemstechtest.util.UnitsUtil

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

private const val KW_UNITS = "kW"

data class CurrentPowerData(
    val solarPower: Double = 0.0,
    val quasarsPower: Double = 0.0,
    val gridPower: Double = 0.0,
    val buildingDemand: Double = 0.0,
    val systemSocPower: Double = 0.0,
    val totalEnergy: Double = 0.0,
    val currentEnergy: Double = 0.0
) {
    fun getSolarPowerTextValue() = "$solarPower $KW_UNITS"
    fun getQuasarPowerTextValue() = "$quasarsPower $KW_UNITS"
    fun getGridPowerTextValue() = "$gridPower $KW_UNITS"
    fun getBuildingDemandTextValue() = "$buildingDemand $KW_UNITS"

    fun gridPowerPercentage(): Double {
        return UnitsUtil.roundWithPrecision((gridPower / buildingDemand) * 100, 2)
    }

    fun solarPowerPercentage(): Double {
        return UnitsUtil.roundWithPrecision((solarPower / buildingDemand) * 100, 2)
    }

    fun quasarPowerPercentage(): Double {
        return UnitsUtil.roundWithPrecision((quasarsPower / buildingDemand) * 100, 2)
    }
}
