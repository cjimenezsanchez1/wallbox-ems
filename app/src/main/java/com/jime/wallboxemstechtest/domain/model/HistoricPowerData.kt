package com.jime.wallboxemstechtest.domain.model

import android.os.Parcel
import android.os.Parcelable

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

data class HistoricPowerData(
    val buildingActivePower: Double = 0.0,
    val gridActivePower: Double = 0.0,
    val solarPanelsActivePower: Double = 0.0,
    val quasarsActivePower: Double = 0.0,
    val timeStamp: String = "",
) : Parcelable {

    constructor(parcel: Parcel) : this(
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readDouble(),
        parcel.readString() ?: ""
    )

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeDouble(buildingActivePower)
        parcel.writeDouble(gridActivePower)
        parcel.writeDouble(solarPanelsActivePower)
        parcel.writeDouble(quasarsActivePower)
        parcel.writeString(timeStamp)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<HistoricPowerData> {
        override fun createFromParcel(parcel: Parcel): HistoricPowerData {
            return HistoricPowerData(parcel)
        }

        override fun newArray(size: Int): Array<HistoricPowerData?> {
            return arrayOfNulls(size)
        }
    }
}
