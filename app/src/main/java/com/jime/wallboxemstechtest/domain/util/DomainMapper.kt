package com.jime.wallboxemstechtest.domain.util

/**
 * Created by Carlos Jiménez Sánchez  on 17/3/22
 * CJS
 **/

interface DomainMapper <T, DomainModel>{

    fun mapToDomainModel(model: T): DomainModel

    fun mapFromDomainModel(domainModel: DomainModel): T
}
